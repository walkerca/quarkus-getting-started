package me.carlwalker.book;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class BookResourceTest {

    @Test
    public void shouldCreateBook() {

        given()
                .body( "title=AllQuietOnTheWesternFront&price=7.99&description=WorldWarOneFromASoldiersViewpoint")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .when()
                .post("/books")
            .then()
                .assertThat()
                .statusCode(is(200))
                .and()
                .body("title", equalTo("AllQuietOnTheWesternFront"))
                .and()
                .body("price", equalTo(Float.valueOf(7.99f)))
                .and()
                .body("description", equalTo("WorldWarOneFromASoldiersViewpoint"));
    }
}
