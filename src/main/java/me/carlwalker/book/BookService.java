package me.carlwalker.book;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class BookService {


    NumberGenerator numberGenerator;

    public BookService(@ThirteenDigits NumberGenerator numberGenerator) {
        this.numberGenerator = numberGenerator;
    }

    public Book createBook(String title, Float price, String description) {
        Book book = new Book(title, price, description);
        book.setIsbn(numberGenerator.generateNumber());
        return book;
    }
}
