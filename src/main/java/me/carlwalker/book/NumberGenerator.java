package me.carlwalker.book;

public interface NumberGenerator {

    String generateNumber();
}
