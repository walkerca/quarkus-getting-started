package me.carlwalker.book;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/books")
public class BookResource {

    BookService bookService;

    public BookResource(BookService bookService) {
        this.bookService = bookService;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createBook(@FormParam("title") String title,
                               @FormParam("price") String price,
                               @FormParam("description") String description) {

        Book book = bookService.createBook(title, Float.valueOf(price), description);

        return Response
                .ok()
                .entity(book)
                .build();
    }
}
