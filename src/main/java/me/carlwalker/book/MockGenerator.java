package me.carlwalker.book;


import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;

@Alternative
@ThirteenDigits
@ApplicationScoped
public class MockGenerator implements NumberGenerator {
    @Override
    public String generateNumber() {
        return "MOCK";
    }
}
