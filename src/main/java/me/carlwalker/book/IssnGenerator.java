package me.carlwalker.book;

import java.util.Random;

//@ApplicationScoped
@EightDigits
public class IssnGenerator implements NumberGenerator {
    public String generateNumber() {
        return "8-" + Math.abs(new Random().nextInt());
    }
}
