package me.carlwalker;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("/artists")
public class ArtistResource {

    private static final List<Artist> artists = List.of(
            new Artist().withId(UUID.randomUUID()).withFirstName("John").withLastName("Lennon"),
            new Artist().withId(UUID.randomUUID()).withFirstName("Paul").withLastName("McCartney"),
            new Artist().withId(UUID.randomUUID()).withFirstName("George").withLastName("Harrison"),
            new Artist().withId(UUID.randomUUID()).withFirstName("Ringo").withLastName("Starr")
    );

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllArtists() {
        return Response.ok(artists).build();
    }

    @GET
    @Path("/count")
    @Produces(MediaType.TEXT_PLAIN)
    public Integer countArtists() {
        return artists.size();
    }
}